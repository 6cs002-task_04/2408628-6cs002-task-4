package Task_04;

import java.lang.reflect.Field;

public class Reflection07 {
  public static void main(String[] args) throws Exception {
    Simple s = new Simple();
    Field[] fields = s.getClass().getDeclaredFields();
    System.out.printf("There are %d fields\n", fields.length);

    for (Field f : fields) {
      f.setAccessible(true);
      System.out.printf("field name=%s type=%s value=%f\n", f.getName(),
          f.getType(), f.getDouble(s));
    }
  }
}
